/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denstreamalgorithm;

import java.util.ArrayList;

/**
 *
 * @author Mochamad Yusuf A A
 */
public class Titik {
    private short titik_a;
    private short titik_b;
    private float distance;
    public Titik(int titik_a,int titik_b,ArrayList<Dataset> mDataset){
        this.titik_a = (short) titik_a;
        this.titik_b = (short) titik_b;
        int a = mDataset.get(titik_a).getFitur5() - mDataset.get(titik_b).getFitur5();
        int b = mDataset.get(titik_a).getFitur3() - mDataset.get(titik_b).getFitur3();
        int c = mDataset.get(titik_a).getFitur4() - mDataset.get(titik_b).getFitur4();
        a *= a; /* a = a*a; artinya itu nilai a sekarang jadi a*a alias a^2 */
        b *= b;
        c *= c;
        distance = (float) Math.sqrt(a+b+c);
    }

    public int getTitik_a() {
        return titik_a;
    }

    public int getTitik_b() {
        return titik_b;
    }

    public double getDistance() {
        return distance;
    }
    
}
