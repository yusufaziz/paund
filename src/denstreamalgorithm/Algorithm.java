/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denstreamalgorithm;

/**
 *
 * @author Xhizound
 */
import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.math.plot.Plot3DPanel;

public class Algorithm implements Runnable {
    static ArrayList<Titik> mTitik = new ArrayList<>();
    private static void viewRam(){
        long x = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("Memory terpakai: "+ x/(1024*1024));
    }
    static void DatasetFromFile(){
        Global.status = "Getting Dataset From File" ;
        String[] theLine,dataset;
        String nextLine;
        FileInputStream file = null;
        BufferedReader reader = null;
        try{
            file = new FileInputStream(Global.INPUT_CSV);  
        }catch(Exception ex){
            
        }
        int urut = 0;
        try {
            reader = new BufferedReader(new InputStreamReader(file));
            while ((nextLine = reader.readLine()) != null) {
                    theLine = nextLine.split("\t");
                    for (String token : theLine) {
                        //split per koma, lalu masuk ke array
                        Dataset DS = new Dataset();
                        dataset = token.split(",");
                        /* take data from colum in csv */
                        DS.setNo(urut);
                        if(Global.modelDataset.equalsIgnoreCase("Darpa")){
                            /*  TYPE, LOOK ALL              Sesuaikan dengan di csv
                                FUNC IN Dataset.java        Ada di kolom apa datanya */
                            DS.setSource(                   dataset[Global.COLUMN_A]);
                            DS.setDestination(              dataset[Global.COLUMN_B]);
                            DS.setTime(Double.valueOf(      dataset[Global.COLUMN_C]));
                            DS.setFitur1(Integer.valueOf(   dataset[Global.COLUMN_D]));
                            DS.setFitur2(Integer.valueOf(   dataset[Global.COLUMN_E]));
                            DS.setFitur3(Integer.valueOf(   dataset[Global.COLUMN_F]));
                            DS.setFitur4(Integer.valueOf(   dataset[Global.COLUMN_G]));
                            DS.setFitur5(Integer.valueOf(   dataset[Global.COLUMN_H]));
                            DS.setFitur6(Integer.valueOf(   dataset[Global.COLUMN_I]));
                            DS.setFitur7(Integer.valueOf(   dataset[Global.COLUMN_J]));
                            DS.setFitur8(Integer.valueOf(   dataset[Global.COLUMN_K]));
                            DS.setFitur9(Integer.valueOf(   dataset[Global.COLUMN_L]));
                            DS.setNama(                     dataset[Global.COLUMN_M]);
                        }else if(Global.modelDataset.equalsIgnoreCase("KDD")){
                            
                        }
                        Global.mDataset.add(DS);
                        urut++;
                    }
            }
        } catch (IOException ex) {
            Logger.getLogger(Algorithm.class.getName()).log(Level.SEVERE, null, ex);
        }
        GUI.addInfo("Read File Sucess");
    }
    
    public static void PreprocessingToFile() {
        Global.status = "Creating Preprocessing File";
        try {
            FileWriter fileWriter = new FileWriter(Global.PREPROCESING_CSV);
            //Write the CSV file header
            fileWriter.append(Global.FILE_HEADER);
            //Add a new line separator after the header
            fileWriter.append(Global.NEW_LINE_SEPARATOR);
            for (int w = 0; w < Global.mDataset.size(); w++) {
                /* including in this looping, all dataset already been created */
                Global.mDataset.get(w).calcCoordinat();
                double[] coordinat = Global.mDataset.get(w).getCoordinat();
                //System.out.println("Coordinat dataset ke "+w +" > x:"+coordinat[Global.X]+" y:"+coordinat[Global.Y]+" z:"+coordinat[Global.Z]);
                fileWriter.append(String.valueOf(Global.mDataset.get(w).getFitur1()));
                fileWriter.append(Global.COMMA_DELIMITER);
                fileWriter.append(String.valueOf(Global.mDataset.get(w).getFitur2()));
                fileWriter.append(Global.COMMA_DELIMITER);
                fileWriter.append(String.valueOf(Global.mDataset.get(w).getFitur3()));
                fileWriter.append(Global.COMMA_DELIMITER);
                fileWriter.append(String.valueOf(Global.mDataset.get(w).getFitur4()));
                fileWriter.append(Global.COMMA_DELIMITER);
                fileWriter.append(String.valueOf(Global.mDataset.get(w).getFitur5()));
                fileWriter.append(Global.COMMA_DELIMITER);
                fileWriter.append(String.valueOf(Global.mDataset.get(w).getFitur6()));
                fileWriter.append(Global.COMMA_DELIMITER);
                fileWriter.append(String.valueOf(Global.mDataset.get(w).getFitur7()));
                fileWriter.append(Global.COMMA_DELIMITER);
                fileWriter.append(Global.mDataset.get(w).getNama());
                fileWriter.append(Global.NEW_LINE_SEPARATOR);
            }
            GUI.addInfo("Preprocessing File Created ");
            System.out.println("CSV file was created successfully !!!");
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error writing CSV file !!!");
            e.printStackTrace();
        }
    }
    public static double getDistance(Dataset titikx,Dataset titiky){
        Global.status = "Calculating distance";
        double a = titikx.getFitur1() - titiky.getFitur1();
        double b = titikx.getFitur2() - titiky.getFitur2();
        double c = titikx.getFitur3() - titiky.getFitur3();
        a*=a;b*=b;c*=c;
        double distance = Math.sqrt(a+b+c);
        // System.out.println("jarak:"+titikx.getNo()+" ke "+titiky.getNo()+"= "+distance);
        return distance;                 
    }
    public static void draw_mDataset(){
        Plot3DPanel realLabel = new Plot3DPanel();
        double min[] = {0,0,0};
        double max[] = {100,10,10};
        realLabel.setFixedBounds(min, max);
        double x_mDataset[] = new double[Global.mDataset.size()];
        double y_mDataset[] = new double[Global.mDataset.size()];
        double z_mDataset[] = new double[Global.mDataset.size()];
        for(int i=0;i<Global.mDataset.size();i++){
            double coordinat[] = Global.mDataset.get(i).getCoordinat();
            x_mDataset[i] = coordinat[Global.X];
            y_mDataset[i] = coordinat[Global.Y];
            z_mDataset[i] = coordinat[Global.Z];
        }
        realLabel.addScatterPlot("Scater",Color.GREEN,x_mDataset,y_mDataset,z_mDataset);
        JFrame frame = new JFrame("Plotting Dataset");
        frame.setSize(800, 600);
        frame.setContentPane(realLabel);
        frame.setVisible(true);
    }
    public static void draw_mCluster(){
        Plot3DPanel scat = new Plot3DPanel();
        double min[] = {0,0,0};
        double max[] = {100,10,10};
        scat.setFixedBounds(min, max);
        for(int i=0;i<Global.mCluster.size();i++){
            Cluster cluster = Global.mCluster.get(i);
            double[] x = new double[cluster.clust.size()];
            double[] y = new double[cluster.clust.size()];
            double[] z = new double[cluster.clust.size()];
            for(int j=0;j<cluster.clust.size();j++){
                Dataset ds = cluster.clust.get(j);
                double coordinat[] = ds.getCoordinat();
                x[j]=coordinat[Global.X];
                y[j]=coordinat[Global.Y];
                z[j]=coordinat[Global.Z];
            }
            Color randomCol = Color.getHSBColor((float) Math.random(), (float) Math.random(), (float) Math.random());
            String title = "Cluster ke: " + i;
            scat.addScatterPlot(title,randomCol, x, y, z);
        }
        JFrame frame = new JFrame("Plotting Cluster");
        frame.setSize(800, 600);
        frame.setContentPane(scat);
        frame.setVisible(true);
    }
    
    private static void DBSCAN(){
        for(int i = 0;i<Global.maxDBSCAN;i++){
            Dataset DS = new Dataset();
            DS = Global.getDatasetFromNo(i);
            if(!DS.isDbscanVisit()){
                ArrayList<Dataset> NeighborPts = regionQuery(DS);
                while(true){
                    if(NeighborPts.isEmpty()){
                        // do nothing
                    }else{
                        break;
                    }
                }
                if(NeighborPts.size() < Global.minpts){
                    DS.setNoise(true);
                }else{
                    Cluster nextCluster = new Cluster();
                    expandCluster(DS,NeighborPts,nextCluster);
                    Global.mCluster.add(nextCluster);
                }
            }
        }
    }
    private static ArrayList<Dataset> regionQuery(Dataset IN){
        //System.out.println("++++++ REGION QUERY START +++++++");
        ArrayList<Dataset> x = new ArrayList<Dataset>();
        ArrayList<Thread> mThread = new ArrayList<Thread>();
        //if(Global.maxDBSCAN < 500){
        if(true){
            for(int i = 0;i<Global.maxDBSCAN;i++){
                Dataset DS = new Dataset();
                DS = Global.getDatasetFromNo(i);
                if(getDistance(DS,IN)<Global.epsilon ){
                    x.add(DS);
                }
            }
        }else {
            int start = 0;
            int end = 0;
            int diff = Global.maxDBSCAN / Global.maxThread;
            for(int i = 0;i<Global.maxThread;i++){
                end += diff;
                QueryThread qt = new QueryThread(start,end,IN,x);
                start = end;
                Thread th = new Thread(qt);
                // th.setPriority(Thread.MAX_PRIORITY);
                mThread.add(th);
            }
            //System.out.println("# ALL THREAD STARTED");
            for(int i = 0;i<Global.maxThread;i++){
                mThread.get(i).start();
            }
            for(int i = 0;i<Global.maxThread;i++){
                try{
                    mThread.get(i).join();
                }catch(Exception e){
                    
                }
            }
            int counter = 0;
            while(counter < Global.maxThread){
                for(int i = 0;i<Global.maxThread;i++){
                try{
                    if(!mThread.get(i).isAlive()){
                        counter++;
                    }
                }catch(Exception e){
                    
                }
                
            }
            }
            //System.out.println("# ALL THREAD JOINED");
        }
        //System.out.println("++++++ REGION QUERY END +++++++");
        return x;
    }
    private static void expandCluster(Dataset IN, ArrayList<Dataset> NeighborPts,Cluster cluster){
        cluster.addDataset(IN);
        int current = 0;
        while(true){
            Dataset DS = new Dataset();
            try{
                DS = NeighborPts.get(current);
            }catch(Exception ex){
                System.out.println("Exception in neighbor dataset with index : " + current);
                break;
            }
            if(!DS.isDbscanVisit()){
                DS.setDbscanVisit(true);
                ArrayList<Dataset> _NeighborPts = regionQuery(DS);
                if(_NeighborPts.size() >= Global.minpts){
                    joinNeighbor(NeighborPts,_NeighborPts);
                }
            }
            if(!Global.ClusterDataExist(DS,null)){
                cluster.addDataset(DS);
            }
            current++;
            if(current >= NeighborPts.size()){
                break;
            }
        }
    }
    private static void joinNeighbor(ArrayList<Dataset> NeighborPts,ArrayList<Dataset> _NeighborPts){
        for(int i=0;i<_NeighborPts.size();i++){
            Dataset DS = new Dataset();
            DS = _NeighborPts.get(i);
            if(!NeighborPts.contains(DS)){
                NeighborPts.add(DS);
            }
        }
    }
    private static void DenstreamAlgorithm(){
        Global.status = "Denstream Algorithm";
        /* example of adding new dataset to existing cluster */
        Dataset newDataset = new Dataset();
        /* ini isi aja, dataset barunya apa */
        if(newDataset.getTime()%Global.getTP() == 0){
            for(int i=0;i<Global.mCluster.size();i++){
                if(Global.mCluster.get(i).isPucluster()){
                    if(Global.mCluster.get(i).wheight < (Global.beta * Global.mu)){
                        Global.mCluster.remove(i);
                    }
                }else{
                    double pangkatPembilang = -Global.lamda*(newDataset.getTime()-Global.mCluster.get(i).getCreationTime()+Global.getTP());
                    double pembilang = Math.pow(2, pangkatPembilang)-1;
                    double pangkatPenyebut = (-Global.lamda)*Global.getTP();
                    double penyebut = Math.pow(2,pangkatPenyebut)-1;
                    double xi = pembilang / penyebut;
                    if(Global.mCluster.get(i).wheight < xi){
                        Global.mCluster.remove(i);
                    }
                }
            }
        }
    }
    public void run(){
        System.out.println("Thread start");
        Global.status = "Starting Application";
        Global.getTP();
        Global.progressBar = 5;
        /* reading data from csv then include to mDataset */
        DatasetFromFile();
        Global.progressBar = 15;
        /* write preprocessing file */
        PreprocessingToFile();
        Global.progressBar = 20;
        /* ALGORITHM PROCESS */
        DBSCAN();
        //DBScan();
        //clusterDBScan();
        Global.progressBar = 50;
        draw_mDataset();
        Global.progressBar = 60;
        draw_mCluster();
        Global.progressBar = 70;
        DenstreamAlgorithm();
        
        Global.progressBar = 100;
        GUI.addInfo("File Processing Done!!");
        GUI.addInfo("____________________");
        GUI.addInfo("Jumlah Cluster :"+String.valueOf(Global.mCluster.size()));
        Global.status = "Starting Application";
        Global.status = "All Proccesing Done!";
    }
}