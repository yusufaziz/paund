/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denstreamalgorithm;

/**
 *
 * @author Xhizound
 */

public class Counting {

    static int w = 0;

    public static void counting(Object[] elements, int K) {
        w = 0;
        int N = elements.length;

        if (K > N) {
            System.out.println("Invalid input, K > N");
            return;
        }
        c(N, K);
        int combination[] = new int[K];
        int r = 0;
        int index = 0;

        while (r >= 0) {
			// possible indexes for 1st position "r=0" are "0,1,2" --> "A,B,C"
            // possible indexes for 2nd position "r=1" are "1,2,3" --> "B,C,D"

            // for r = 0 ==> index < (4+ (0 - 2)) = 2
            if (index <= (N + (r - K))) {
                combination[r] = index;
                // if we are at the last position print and increase the index
                if (r == K - 1) {
                    //do something with the combination e.g. add to list or print
                    index++;
                } else {
                    // select index for next position
                    index = combination[r] + 1;
                    r++;
                }
            } else {
                r--;
                if (r > 0) {
                    index = combination[r] + 1;
                } else {
                    index = combination[0] + 1;
                }
            }
        }
    }

    public static int c(int n, int r) {
        int nf = fact(n);
        int rf = fact(r);
        int nrf = fact(n - r);
        int npr = nf / nrf;
        int ncr = npr / rf;
        return ncr;
    }

    public static int fact(int n) {
        if (n == 0) {
            return 1;
        } else {
            return n * fact(n - 1);
        }
    }

    public static void resultCombination() {
        //combination
        w = 0;
        //MANUAL : Ubah jika jumlah fitur berubah
        Object[] elements = new Object[]{'0', '1', '2', '3'};
        counting(elements, 3);
    }
}
