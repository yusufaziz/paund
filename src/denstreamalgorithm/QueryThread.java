/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denstreamalgorithm;
import java.util.ArrayList;

/**
 *
 * @author Mochamad Yusuf A A
 */
public class QueryThread implements Runnable {
    int start;
    int end;
    Dataset IN;
    ArrayList<Dataset> x;
    public QueryThread(int start,int end,Dataset IN, ArrayList<Dataset> x) {
        this.start = start;
        this.end = end;
        this.IN = IN;
        this.x = x;
    }

    public void splitQuery(){
        //System.out.println("Thread start :"+start+" Started");
        if(end > Global.maxDBSCAN){
            end = Global.maxDBSCAN;
        }
        for(int i = start;i<end;i++){
            Dataset DS = new Dataset();
            DS = Global.getDatasetFromNo(i);
            if(Algorithm.getDistance(DS,IN)<Global.epsilon ){
                while(true){
                    try{
                        x.add(DS);
                        break;
                    }catch(Exception ex){
                        System.out.println("Waiting for insert dataset");
                    }
                }
            }
        }
        //System.out.println("Thread start :"+start+" Ended");
    }

    @Override
    public void run() {
        splitQuery();
    }
}
