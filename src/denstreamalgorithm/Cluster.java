/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package denstreamalgorithm;

import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author Xhizound
 */
public class Cluster {
    ArrayList<Dataset> clust = new ArrayList<>();
    double wheight = 0;
    double radius = 0;
    boolean first = false;
    boolean pucluster = false;
    double creationTime = 0;
    
    public ArrayList getCluster(){
        return clust;
    }
    public Dataset getDataset(int index){
        return clust.get(index);
    }
    public double getCreationTime() {
        if(this.creationTime == 0){
            double temp=0;
            for(int i = 0;i<clust.size();i++){
                if(clust.get(i).getTime()>temp){
                    temp = clust.get(i).getTime();
                }
            }
            this.creationTime = temp;
        }
        return creationTime;
    }
    public boolean isPucluster() {
        return pucluster;
    }

    public void setPucluster(boolean pucluster) {
        this.pucluster = pucluster;
    }
    
    public boolean addDataset(Dataset datasetBaru){
        boolean ret = false;
        if(!clust.contains(datasetBaru)){
            clust.add(datasetBaru);
        }
        return ret;
    }
    public void calcWheight(){
        this.wheight = 0;
        for (int i = 0; i < clust.size(); i++){
            double a = (clust.get(i).getFitur1()+clust.get(i).getFitur2()+clust.get(i).getFitur3());
            double selisih = 0;
            if(clust.get(i).getNo() != (Global.mDataset.size()-2)){
                selisih = Global.mDataset.get(clust.get(i).getNo()+1).getTime()-clust.get(i).getTime();
            }
            double pangkat = ((-Global.lamda)*selisih);
            this.wheight += (a*Math.pow(2, pangkat));
        }
    }
    public void calcRadius(){
        double cf1 = 0;
        double cf2 = 0;
        this.radius = 0;
        for (int i = 0; i < clust.size(); i++){
            double selisih = 0;
            if(clust.get(i).getNo() > 0){
                selisih = clust.get(i).getTime()-Global.mDataset.get(clust.get(i).getNo()-1).getTime();
            }
            int totalFitur = (   Global.mDataset.get(i).getFitur1()+
                                Global.mDataset.get(i).getFitur2()+
                                Global.mDataset.get(i).getFitur3()); 
            cf1 += selisih * totalFitur;
            cf2 += selisih * (totalFitur*totalFitur);
        }
        double a = (cf2/this.wheight) - ((cf1/this.wheight)*(cf1/this.wheight));
        this.radius = Math.sqrt(a);
    }
    public boolean addNewDataset(Dataset newDataset){
        boolean status = false;
        double lastW = this.wheight, lastR = this.radius;
        double selisih = 0;
        if(newDataset.getNo() > 0){
            selisih = newDataset.getTime() - Global.mDataset.get(newDataset.getNo()-1).getTime();
        }
        double fading = this.wheight * Math.pow(2, ((-Global.lamda)*selisih));
        this.wheight *= fading;
        this.wheight += newDataset.getWheight();
        this.calcRadius();
        if(this.radius < Global.epsilon){
            clust.add(newDataset);
            status = true;
        }else{
            this.radius = lastR;
            this.wheight = lastW;
        }
        return status;
    }
}
