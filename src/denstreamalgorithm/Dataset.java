/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denstreamalgorithm;

/**
 *
 * @author Mochamad Yusuf A A
 */
public class Dataset {

    private int no,
            protocol,
            length,
            fitur1,
            fitur2,
            fitur3,
            fitur4,
            fitur5,
            fitur6,
            fitur7,
            fitur8,
            fitur9,
            fitur10,
            fitur11;
    /* DBSCAN FLAG */
    boolean dbscanVisit=false,noise=false;

    public boolean isDbscanVisit() {
        return dbscanVisit;
    }

    public void setDbscanVisit(boolean dbscanVisit) {
        this.dbscanVisit = dbscanVisit;
    }

    public boolean isNoise() {
        return noise;
    }

    public void setNoise(boolean noise) {
        this.noise = noise;
    }
    
    public int getFitur1() {
        return fitur1;
    }

    public void setFitur1(int fitur1) {
        this.fitur1 = fitur1;
    }

    public int getFitur2() {
        return fitur2;
    }

    public void setFitur2(int fitur2) {
        this.fitur2 = fitur2;
    }

    public int getFitur3() {
        return fitur3;
    }

    public void setFitur3(int fitur3) {
        this.fitur3 = fitur3;
    }

    public int getFitur4() {
        return fitur4;
    }

    public void setFitur4(int fitur4) {
        this.fitur4 = fitur4;
    }

    public int getFitur5() {
        return fitur5;
    }

    public void setFitur5(int fitur5) {
        this.fitur5 = fitur5;
    }

    public int getFitur6() {
        return fitur6;
    }

    public void setFitur6(int fitur6) {
        this.fitur6 = fitur6;
    }

    public int getFitur7() {
        return fitur7;
    }

    public void setFitur7(int fitur7) {
        this.fitur7 = fitur7;
    }

    public int getFitur8() {
        return fitur8;
    }

    public void setFitur8(int fitur8) {
        this.fitur8 = fitur8;
    }

    public int getFitur9() {
        return fitur9;
    }

    public void setFitur9(int fitur9) {
        this.fitur9 = fitur9;
    }

    public int getFitur10() {
        return fitur10;
    }

    public void setFitur10(int fitur10) {
        this.fitur10 = fitur10;
    }

    public int getFitur11() {
        return fitur11;
    }

    public void setFitur11(int fitur11) {
        this.fitur11 = fitur11;
    }
    
    
    private double time,
            process_time,wheight=0;
    private String source,
            destination,
            info,
            nama;

    public double getWheight() {
        if(wheight == 0){
            double selisih = 0;
            if(this.getNo() > 0){
                selisih = this.getTime() - Global.mDataset.get(this.getNo()-1).getTime();
            }
            double wx = (this.getFitur1() * Math.pow(2, ((-Global.lamda)*selisih)));
            double wy = (this.getFitur2() * Math.pow(2, ((-Global.lamda)*selisih)));
            double wz = (this.getFitur3() * Math.pow(2, ((-Global.lamda)*selisih)));
            double w_total = wx+wy+wz;
            setWheight(w_total);
        }
        return wheight;
    }

    public void setWheight(double wheight) {
        this.wheight = wheight;
    }
    
    double coordinat[] = new double[Global._3D];
    public Dataset() {
        
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getProtocol() {
        return protocol;
    }

    public void setProtocol(int protocol) {
        this.protocol = protocol;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getProcess_time() {
        return process_time;
    }

    public void setProcess_time(double process_time) {
        this.process_time = process_time;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }   

    public double[] getCoordinat() {
        return coordinat;
    }

    public void calcCoordinat() {
        double selisih = 0;
        if(this.getNo() > 0){
            selisih = this.getTime() - Global.mDataset.get(this.getNo()-1).getTime();
        }
        double wx = (this.getFitur1() * Math.pow(2, ((-Global.lamda)*selisih)));
        double wy = (this.getFitur2() * Math.pow(2, ((-Global.lamda)*selisih)));
        double wz = (this.getFitur3() * Math.pow(2, ((-Global.lamda)*selisih)));
        double w_total = wx+wy+wz;
        setWheight(w_total);
        double x = (wx*this.getFitur1())/(w_total);
        double y = (wy*this.getFitur2())/(w_total);
        double z = (wz*this.getFitur3())/(w_total);
        this.coordinat[Global.X] = x;
        this.coordinat[Global.Y] = y;
        this.coordinat[Global.Z] = z;
    }
    public double[] get2dCoordinate(){
        /* transform using https://en.wikipedia.org/wiki/3D_projection */
        double coordinate[] = new double[Global._2D];
        //point a (3D)
        double ax = this.coordinat[Global.X];
        double ay = this.coordinat[Global.Y];
        double az = this.coordinat[Global.Z];

        //point b (2D)
        double bx = 0;
        double by = 0;

        //assumtions
        double cx = 0; //offset
        double cz = 0; //offset

        double scalex = 0.5;
        double scalez = 0.5;

        //trafo 3D -> 2D
        bx = scalex * ax + cz;
        by = scalez * az + cz;
        return coordinate;
    }
        
}
