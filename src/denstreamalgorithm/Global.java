/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denstreamalgorithm;

import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author Mochamad Yusuf A A
 */
public class Global {
    /* CSV */
    public static String PREPROCESING_CSV = "Preprocessing1000.csv";
    public static String OUTPUT_CSV = "Output.csv";
    public static String INPUT_CSV = "dataset/10.000normalDarpa.csv";
    public static String ALGORITHM_CSV = "Algorithm.csv";
    public static final int COLUMN_A = 0;
    public static final int COLUMN_B = 1;
    public static final int COLUMN_C = 2;
    public static final int COLUMN_D = 3;
    public static final int COLUMN_E = 4;
    public static final int COLUMN_F = 5;
    public static final int COLUMN_G = 6;
    public static final int COLUMN_H = 7;
    public static final int COLUMN_I = 8;
    public static final int COLUMN_J = 9;
    public static final int COLUMN_K = 10;
    public static final int COLUMN_L = 11;
    public static final int COLUMN_M = 12;
    public static final int COLUMN_N = 13;
    
    /* TIME */
    public static double PROCESS_TIME = 0.01;
    
    /* COUNTER INFORMATION */
    public static int cSource = 0;
    public static int cDestination = 0;
    public static int cProtocol = 0;
    public static int cSYN = 0;
    public static int cACK = 0;
    public static int cPort = 0;
    public static int cLength = 0;
       
    /* ALGORITHM COEEFICIENT */
    public static double epsilon = 1.5;
    public static double minpts = 1;
    public static double lamda = 0.5;
    public static double beta = 2;
    public static double mu = 3;
    public static String jenisDataset;
    public static String modelDataset;
    public static int maxDBSCAN = 2000;
    
    /* Mapping Coordinat */
    public static int _3D = 3;
    public static int _2D = 2;
    public static int X = 0;
    public static int Y = 1;
    public static int Z = 2;
    
    /* OTHER */
    public static final String COMMA_DELIMITER = ",";
    public static final String NEW_LINE_SEPARATOR = "\n";
    public static final String FILE_HEADER = "IP_Source,IP_Destination,Protocol,SYN,ACK,Port_Out,Length,Label";
    public static final String FILE_HEADER2 = "No_Window, Cluster, Centroid, Label, True_Negative, False_Negative, True_Positive, False_Positive, Detection_Rate, False_Positive_Rate, Accuracy, Sum_of_Squared_Error";
      
    /* GUI DATA */
    public static String status = "Application is Not Running";
    public static String exTime = "";
    public static int progressBar = 0;
    public static int maxThread = 2;
    
    /* DATA */
    public static ArrayList<Dataset> mDataset = new ArrayList<>();
    public static ArrayList<Cluster> mCluster = new ArrayList<>();
    public static int getTP(){
        Global.status = "Updating TP";
        double a = (1/lamda);
        double bm = (beta*mu);
        double log = (bm/(bm-1));
        int ceil = (int) Math.ceil(Math.log(log));
        /* debug purpose */
        //System.out.println("a: " + a + " b: "+bm + " log:" + log + " mlog: "+ceil);
        return ceil;
    }
    public static Dataset getDatasetFromNo(int index){
        Dataset ret = new Dataset();
        for(int i=0;i<mDataset.size();i++){
            if(mDataset.get(i).getNo()==index){
                ret = mDataset.get(i);
                break;
            }
        }
        return ret;
    }
    public static boolean ClusterDataExist(Dataset ds,Cluster x){
        boolean ret = false;
        if(x == null){
            for(int i=0;i<mCluster.size();i++){
                Cluster cluster = mCluster.get(i);
                if(cluster.clust.contains(ds)){
                    ret = true;
                }
            }
        }else{
            if(x.clust.contains(ds)){
                ret = true;
            }
        }
        return ret;
    }
}
